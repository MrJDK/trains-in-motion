package ebf.tim.blocks.rails;

import ebf.tim.blocks.RailTileEntity;
import net.minecraft.block.BlockRailBase;
import net.minecraft.world.ChunkPosition;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.List;

public class RailData {
    private World field_150660_b;
    private int field_150661_c;
    private int field_150658_d;
    private int field_150659_e;
    private final boolean field_150656_f;
    private List<ChunkPosition> field_150657_g = new ArrayList<>();
    private final boolean canMakeSlopes;

    public RailData(World p_i45388_2_, int p_i45388_3_, int p_i45388_4_, int p_i45388_5_) {
        this.field_150660_b = p_i45388_2_;
        this.field_150661_c = p_i45388_3_;
        this.field_150658_d = p_i45388_4_;
        this.field_150659_e = p_i45388_5_;
        BlockRailBase block = (BlockRailBase)p_i45388_2_.getBlock(p_i45388_3_, p_i45388_4_, p_i45388_5_);
        int l = block.getBasicRailMetadata(p_i45388_2_, null, p_i45388_3_, p_i45388_4_, p_i45388_5_);
        this.field_150656_f = !block.isFlexibleRail(p_i45388_2_, p_i45388_3_, p_i45388_4_, p_i45388_5_);
        canMakeSlopes = block.canMakeSlopes(p_i45388_2_, p_i45388_3_, p_i45388_4_, p_i45388_5_);
        this.func_150648_a(l);
    }

    private void func_150648_a(int p_150648_1_) {
        this.field_150657_g.clear();

        if (p_150648_1_ == 0) {
            this.field_150657_g.add(new ChunkPosition(this.field_150661_c, this.field_150658_d, this.field_150659_e - 1));
            this.field_150657_g.add(new ChunkPosition(this.field_150661_c, this.field_150658_d, this.field_150659_e + 1));
        }
        else if (p_150648_1_ == 1) {
            this.field_150657_g.add(new ChunkPosition(this.field_150661_c - 1, this.field_150658_d, this.field_150659_e));
            this.field_150657_g.add(new ChunkPosition(this.field_150661_c + 1, this.field_150658_d, this.field_150659_e));
        }
        else if (p_150648_1_ == 2) {
            this.field_150657_g.add(new ChunkPosition(this.field_150661_c - 1, this.field_150658_d, this.field_150659_e));
            this.field_150657_g.add(new ChunkPosition(this.field_150661_c + 1, this.field_150658_d + 1, this.field_150659_e));
        }
        else if (p_150648_1_ == 3) {
            this.field_150657_g.add(new ChunkPosition(this.field_150661_c - 1, this.field_150658_d + 1, this.field_150659_e));
            this.field_150657_g.add(new ChunkPosition(this.field_150661_c + 1, this.field_150658_d, this.field_150659_e));
        }
        else if (p_150648_1_ == 4) {
            this.field_150657_g.add(new ChunkPosition(this.field_150661_c, this.field_150658_d + 1, this.field_150659_e - 1));
            this.field_150657_g.add(new ChunkPosition(this.field_150661_c, this.field_150658_d, this.field_150659_e + 1));
        }
        else if (p_150648_1_ == 5) {
            this.field_150657_g.add(new ChunkPosition(this.field_150661_c, this.field_150658_d, this.field_150659_e - 1));
            this.field_150657_g.add(new ChunkPosition(this.field_150661_c, this.field_150658_d + 1, this.field_150659_e + 1));
        }
        else if (p_150648_1_ == 6) {
            this.field_150657_g.add(new ChunkPosition(this.field_150661_c + 1, this.field_150658_d, this.field_150659_e));
            this.field_150657_g.add(new ChunkPosition(this.field_150661_c, this.field_150658_d, this.field_150659_e + 1));
        }
        else if (p_150648_1_ == 7) {
            this.field_150657_g.add(new ChunkPosition(this.field_150661_c - 1, this.field_150658_d, this.field_150659_e));
            this.field_150657_g.add(new ChunkPosition(this.field_150661_c, this.field_150658_d, this.field_150659_e + 1));
        }
        else if (p_150648_1_ == 8) {
            this.field_150657_g.add(new ChunkPosition(this.field_150661_c - 1, this.field_150658_d, this.field_150659_e));
            this.field_150657_g.add(new ChunkPosition(this.field_150661_c, this.field_150658_d, this.field_150659_e - 1));
        }
        else if (p_150648_1_ == 9) {
            this.field_150657_g.add(new ChunkPosition(this.field_150661_c + 1, this.field_150658_d, this.field_150659_e));
            this.field_150657_g.add(new ChunkPosition(this.field_150661_c, this.field_150658_d, this.field_150659_e - 1));
        }
    }

    private void func_150651_b() {
        for (int i = 0; i < this.field_150657_g.size(); ++i) {
            RailData rail = this.func_150654_a(this.field_150657_g.get(i));

            if (rail != null && rail.func_150653_a(this)) {
                this.field_150657_g.set(i, new ChunkPosition(rail.field_150661_c, rail.field_150658_d, rail.field_150659_e));
            }
            else {
                this.field_150657_g.remove(i--);
            }
        }
    }

    private RailData func_150654_a(ChunkPosition p_150654_1_) {
        return BlockRailBase.func_150049_b_(this.field_150660_b, p_150654_1_.chunkPosX, p_150654_1_.chunkPosY, p_150654_1_.chunkPosZ) ? new RailData(this.field_150660_b, p_150654_1_.chunkPosX, p_150654_1_.chunkPosY, p_150654_1_.chunkPosZ) : (BlockRailBase.func_150049_b_(this.field_150660_b, p_150654_1_.chunkPosX, p_150654_1_.chunkPosY + 1, p_150654_1_.chunkPosZ) ? new RailData(this.field_150660_b, p_150654_1_.chunkPosX, p_150654_1_.chunkPosY + 1, p_150654_1_.chunkPosZ) : (BlockRailBase.func_150049_b_(this.field_150660_b, p_150654_1_.chunkPosX, p_150654_1_.chunkPosY - 1, p_150654_1_.chunkPosZ) ? new RailData(this.field_150660_b, p_150654_1_.chunkPosX, p_150654_1_.chunkPosY - 1, p_150654_1_.chunkPosZ) : null));
    }

    private boolean func_150653_a(RailData p_150653_1_) {
        for (ChunkPosition chunkposition : this.field_150657_g) {
            if (chunkposition.chunkPosX == p_150653_1_.field_150661_c && chunkposition.chunkPosZ == p_150653_1_.field_150659_e) {
                return true;
            }
        }

        return false;
    }

    private boolean func_150652_b(int p_150652_1_, int p_150652_3_) {
        for (ChunkPosition chunkposition : this.field_150657_g) {
            if (chunkposition.chunkPosX == p_150652_1_ && chunkposition.chunkPosZ == p_150652_3_) {
                return true;
            }
        }

        return false;
    }

    private boolean func_150649_b(RailData p_150649_1_) {
        return this.func_150653_a(p_150649_1_) || this.field_150657_g.size() != 2;
    }

    private void func_150645_c(RailData p_150645_1_) {
        this.field_150657_g.add(new ChunkPosition(p_150645_1_.field_150661_c, p_150645_1_.field_150658_d, p_150645_1_.field_150659_e));
        boolean flag = this.func_150652_b(this.field_150661_c, this.field_150659_e - 1);
        boolean flag1 = this.func_150652_b(this.field_150661_c, this.field_150659_e + 1);
        boolean flag2 = this.func_150652_b(this.field_150661_c - 1, this.field_150659_e);
        boolean flag3 = this.func_150652_b(this.field_150661_c + 1, this.field_150659_e);
        byte b0 = -1;

        if (flag || flag1) {
            b0 = 0;
        }

        if (flag2 || flag3) {
            b0 = 1;
        }

        if (!this.field_150656_f) {
            if (flag1 && flag3 && !flag && !flag2) {
                b0 = 6;
            }

            if (flag1 && flag2 && !flag && !flag3) {
                b0 = 7;
            }

            if (flag && flag2 && !flag1 && !flag3) {
                b0 = 8;
            }

            if (flag && flag3 && !flag1 && !flag2) {
                b0 = 9;
            }
        }

        if (b0 == 0 && canMakeSlopes) {
            if (BlockRailBase.func_150049_b_(this.field_150660_b, this.field_150661_c, this.field_150658_d + 1, this.field_150659_e - 1)) {
                b0 = 4;
            }

            if (BlockRailBase.func_150049_b_(this.field_150660_b, this.field_150661_c, this.field_150658_d + 1, this.field_150659_e + 1)) {
                b0 = 5;
            }
        }

        if (b0 == 1 && canMakeSlopes) {
            if (BlockRailBase.func_150049_b_(this.field_150660_b, this.field_150661_c + 1, this.field_150658_d + 1, this.field_150659_e)) {
                b0 = 2;
            }

            if (BlockRailBase.func_150049_b_(this.field_150660_b, this.field_150661_c - 1, this.field_150658_d + 1, this.field_150659_e)) {
                b0 = 3;
            }
        }

        if (b0 < 0) {
            b0 = 0;
        }

        int i = b0;

        if (this.field_150656_f) {
            if(field_150660_b.getTileEntity(this.field_150661_c, this.field_150658_d, this.field_150659_e) instanceof RailTileEntity){
                i= ((RailTileEntity) field_150660_b.getTileEntity(this.field_150661_c, this.field_150658_d, this.field_150659_e)).getMeta() & 8 | b0;
            }
        }

        if(field_150660_b.getTileEntity(this.field_150661_c, this.field_150658_d, this.field_150659_e) instanceof RailTileEntity){
            ((RailTileEntity) field_150660_b.getTileEntity(this.field_150661_c, this.field_150658_d, this.field_150659_e)).setMeta(i);
        }
    }

    private boolean func_150647_c(int p_150647_1_, int p_150647_2_, int p_150647_3_) {
        RailData rail = this.func_150654_a(new ChunkPosition(p_150647_1_, p_150647_2_, p_150647_3_));

        if (rail == null) {
            return false;
        } else {
            rail.func_150651_b();
            return rail.func_150649_b(this);
        }
    }

    public void func_150655_a(boolean p_150655_1_) {
        boolean flag2 = this.func_150647_c(this.field_150661_c, this.field_150658_d, this.field_150659_e - 1);
        boolean flag3 = this.func_150647_c(this.field_150661_c, this.field_150658_d, this.field_150659_e + 1);
        boolean flag4 = this.func_150647_c(this.field_150661_c - 1, this.field_150658_d, this.field_150659_e);
        boolean flag5 = this.func_150647_c(this.field_150661_c + 1, this.field_150658_d, this.field_150659_e);
        byte b0 = -1;

        if ((flag2 || flag3) && !flag4 && !flag5) {
            b0 = 0;
        }

        if ((flag4 || flag5) && !flag2 && !flag3) {
            b0 = 1;
        }

        if (!this.field_150656_f) {
            if (flag3 && flag5 && !flag2 && !flag4) {
                b0 = 6;
            }

            if (flag3 && flag4 && !flag2 && !flag5) {
                b0 = 7;
            }

            if (flag2 && flag4 && !flag3 && !flag5) {
                b0 = 8;
            }

            if (flag2 && flag5 && !flag3 && !flag4) {
                b0 = 9;
            }
        }

        if (b0 == -1) {
            if (flag2 || flag3) {
                b0 = 0;
            }

            if (flag4 || flag5) {
                b0 = 1;
            }

            if (!this.field_150656_f) {
                if (p_150655_1_) {
                    if (flag3 && flag5) {
                        b0 = 6;
                    }

                    if (flag4 && flag3) {
                        b0 = 7;
                    }

                    if (flag5 && flag2) {
                        b0 = 9;
                    }

                    if (flag2 && flag4) {
                        b0 = 8;
                    }
                }
                else {
                    if (flag2 && flag4) {
                        b0 = 8;
                    }

                    if (flag5 && flag2) {
                        b0 = 9;
                    }

                    if (flag4 && flag3) {
                        b0 = 7;
                    }

                    if (flag3 && flag5) {
                        b0 = 6;
                    }
                }
            }
        }

        if (b0 == 0 && canMakeSlopes) {
            if (BlockRailBase.func_150049_b_(this.field_150660_b, this.field_150661_c, this.field_150658_d + 1, this.field_150659_e - 1)) {
                b0 = 4;
            }

            if (BlockRailBase.func_150049_b_(this.field_150660_b, this.field_150661_c, this.field_150658_d + 1, this.field_150659_e + 1)) {
                b0 = 5;
            }
        }

        if (b0 == 1 && canMakeSlopes) {
            if (BlockRailBase.func_150049_b_(this.field_150660_b, this.field_150661_c + 1, this.field_150658_d + 1, this.field_150659_e)) {
                b0 = 2;
            }

            if (BlockRailBase.func_150049_b_(this.field_150660_b, this.field_150661_c - 1, this.field_150658_d + 1, this.field_150659_e)) {
                b0 = 3;
            }
        }

        if (b0 < 0) {
            b0 = 0;
        }

        this.func_150648_a(b0);
        int i = b0;

        if(field_150660_b.getTileEntity(this.field_150661_c, this.field_150658_d, this.field_150659_e) instanceof RailTileEntity) {
            if (this.field_150656_f) {
                i = ((RailTileEntity) field_150660_b.getTileEntity(this.field_150661_c, this.field_150658_d, this.field_150659_e)).getMeta() & 8 | b0;
            }

            ((RailTileEntity) field_150660_b.getTileEntity(this.field_150661_c, this.field_150658_d, this.field_150659_e)).setMeta(i);

            for (ChunkPosition chunkPosition : this.field_150657_g) {
                RailData rail = this.func_150654_a(chunkPosition);

                if (rail != null) {
                    rail.func_150651_b();

                    if (rail.func_150649_b(this)) {
                        rail.func_150645_c(this);
                    }
                }
            }
        }
    }
}
