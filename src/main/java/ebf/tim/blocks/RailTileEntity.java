package ebf.tim.blocks;

import ebf.XmlBuilder;
import ebf.tim.blocks.rails.BlockRailCore;
import ebf.tim.blocks.rails.RailData;
import ebf.tim.blocks.rails.RailShapeCore;
import ebf.tim.models.rails.Model1x1Rail;
import ebf.tim.utility.DebugUtil;
import fexcraft.tmt.slim.TextureManager;
import net.minecraft.block.Block;
import net.minecraft.crash.CrashReportCategory;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

import javax.annotation.Nullable;


public class RailTileEntity extends TileEntity {

    private AxisAlignedBB boundingBox = null;
    //management variables
    //todo public int snow=0;
    //todo public int timer=0;
    //todo public int overgrowth=0;
    public Integer railGLID=null;
    private int meta=0;
    public XmlBuilder data = new XmlBuilder();
    //used for the actual path, and rendered
    //public RailShapeCore points = new RailShapeCore();
    //TODO: only rendered, to show other paths, maybe rework so they are all in the same list and just have a bool for which is active?
    //public List<RailPointData> cosmeticPoints = new ArrayList<>();


    public int getMeta() {
        return meta;
    }
    public void setMeta(int i){
        meta=i;
        markDirty();

    }

    public void func_145828_a(@Nullable CrashReportCategory report)  {
        if (report == null) {
            if (!worldObj.isRemote) {
                return;
            }
            TextureManager.adjustLightFixture(worldObj,xCoord,yCoord,zCoord);
            if(railGLID!=null){
                org.lwjgl.opengl.GL11.glCallList(railGLID);
            }

            if(railGLID==null){
                //todo: compensation for older maps, should be removed after initial release.
                if(data.getfloat("scale")==null && worldObj.getBlock(xCoord, yCoord, zCoord) instanceof BlockRailCore){
                    data.putFloat("scale", ((BlockRailCore)worldObj.getBlock(xCoord, yCoord, zCoord)).renderScale);
                } else {
                    return;
                }
                RailShapeCore route =new RailShapeCore().parseString(data.getString("route"));
                if (route!=null) {
                    if(railGLID==null) {
                        railGLID = net.minecraft.client.renderer.GLAllocation.generateDisplayLists(1);
                    }
                    org.lwjgl.opengl.GL11.glNewList(railGLID, org.lwjgl.opengl.GL11.GL_COMPILE);

                    Model1x1Rail.Model3DRail(worldObj, xCoord, yCoord, zCoord,
                            route,
                            data.getfloat("scale"),
                            data.getItemStack("ballast"),
                            data.getItemStack("ties"),
                            data.getItemStack("rail"));

                    org.lwjgl.opengl.GL11.glEndList();
                } // else {DebugUtil.println("NO DATA");}
            }
        } else {super.func_145828_a(report);}
    }

    @Override
    public boolean shouldRefresh(Block oldBlock, Block newBlock, int oldMeta, int newMeta, World world, int x, int y, int z) {
        return false;
    }

    @Override
    public boolean canUpdate(){return false;}

    @Override
    public void updateEntity(){}

    @Override
    public AxisAlignedBB getRenderBoundingBox() {
        if (boundingBox == null) {
            boundingBox = AxisAlignedBB.getBoundingBox(xCoord, yCoord, zCoord, xCoord+1, yCoord, zCoord+1);
        }
        return boundingBox;
    }


    public void markDirty() {
        super.markDirty();
        if (this.worldObj != null) {
            worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
            this.worldObj.func_147453_f(this.xCoord, this.yCoord, this.zCoord, this.getBlockType());
            railGLID=null;
        }

    }

    @Override
    public S35PacketUpdateTileEntity getDescriptionPacket() {
        NBTTagCompound nbttagcompound = new NBTTagCompound();
        this.writeToNBT(nbttagcompound);
        return new S35PacketUpdateTileEntity(this.xCoord, this.yCoord, this.zCoord, 0, nbttagcompound);
    }

    @Override
    public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt) {
        super.onDataPacket(net, pkt);
        if(pkt ==null){return;}
        readFromNBT(pkt.func_148857_g());
        markDirty();
    }


    @Override
    public void writeToNBT(NBTTagCompound tag){
        super.writeToNBT(tag);
        if(data.getfloat("scale")==null){//should be redundant, but currently necessary for backwards compatibility
            data.putFloat("scale",((BlockRailCore)worldObj.getBlock(xCoord,yCoord,zCoord)).renderScale);
        }
        tag.setString("data", data.toXMLString());
        tag.setInteger("meta", meta);
    }

    @Override
    public void readFromNBT(NBTTagCompound tag){
        if(tag==null){return;}
        super.readFromNBT(tag);
        data = new XmlBuilder(tag.getString("data"));
        if(tag.hasKey("meta")) {
            meta = tag.getInteger("meta");
        } else {
            new RailData(worldObj,xCoord,yCoord,zCoord).func_150655_a(worldObj.isBlockIndirectlyGettingPowered(xCoord,yCoord,zCoord));
        }
    }

}
